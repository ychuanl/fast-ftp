# fast-ftp

#### 介绍
精简ftp服务端,双击启动即可创建一个ftp服务端用来局域网传输文件使用,没有任何驻留服务
支持win和linux，发行版中可下载

[rust 带gui的版本](https://gitee.com/ychuanl/ftp-server)


#### 使用说明

1. 首次启动会在当前目录创建config.properties文件，里面记录了账号密码信息，如果要修改ftp配置信息，修改配置文件即可。
2. 默认根目录为程序所在的盘符，例如你在D盘某个目录下启动则ftp访问目录就是D:/
3. 注意：目前不支持自定义端口，默认端口为2121
4. 默认账号密码 admin admin

